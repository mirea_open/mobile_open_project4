package ru.mirea.vidyakin_i_n.looper;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MyLooper extends Thread {
    public Handler mHandler;
    private Handler mainHandler;

    public MyLooper(Handler mainThreadHandler) {
        mainHandler = mainThreadHandler;
    }

    @Override
    public void run() {
        Log.d("ru.mirea.vidyakin_i_n.looper.MyLooper", "run");
        Looper.prepare();
        mHandler = new Handler(Looper.myLooper()) {
            public void handleMessage(Message msg) {
                String data = msg.getData().getString("KEY");
                int data1 = msg.getData().getInt("Vozr");
                Log.d("ru.mirea.vidyakin_i_n.looper.MyLooper get message: ", data);
                Log.d("ru.mirea.vidyakin_i_n.looper.MyLooper get message: ", String.valueOf(data1));
                Message message = new Message();
                Bundle bundle = new Bundle();
                try {
                    TimeUnit.SECONDS.sleep(data1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                bundle.putString("result", String.format("My age is %d and my work is %s ", data1, data));
                message.setData(bundle);
// Send the message back to main thread message queue use main thread message Handler.
                mainHandler.sendMessage(message);
            }
        };
        Looper.loop();
    }
}